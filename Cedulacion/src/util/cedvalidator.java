/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import static util.dateoperator.getAge;
import static util.dateoperator.isDate;
import static util.dateoperator.stringToDate;


public class cedvalidator {
  public static boolean cedNicaragua(String ced){
        if(ced.length()!= 14){
            return false;
        }
        if(!Character.isLetter(ced.charAt(13))){
            return false;
        }
        
        if(!(Character.isDigit(ced.charAt(0))&&Character.isDigit(ced.charAt(1))&&
           Character.isDigit(ced.charAt(2))&&Character.isDigit(ced.charAt(3))&&     
           Character.isDigit(ced.charAt(4))&&Character.isDigit(ced.charAt(5))&&
           Character.isDigit(ced.charAt(6))&&Character.isDigit(ced.charAt(7))&&     
           Character.isDigit(ced.charAt(8))&&Character.isDigit(ced.charAt(9))&&
           Character.isDigit(ced.charAt(10))&&Character.isDigit(ced.charAt(11))&&
           Character.isDigit(ced.charAt(12)))){
           return false;
        }

        String birthDate = ced.substring(3,9);
        if (!isDate(birthDate,"ddMMyy")) {
            return false;
        }   
        return getAge(stringToDate(Fnacimiento(ced),"dd-mm-yyyy"))>=16;
    }
     public static String Fnacimiento(String ced){
        String day = ced.substring(3, 5);
        String month = ced.substring(5, 7);
        String year = ced.substring(7, 9);
        int y = Integer.parseInt(year);

        if (y >= 0 && y <= 29) {
            y += 2000;
        } else {
            y += 1900;
        }
        String date= String.valueOf(y)+"-"+month+ "-" + day;
        return date;    
    }

  
}

