/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import models.Modelos;
import views.Datospersonales;

/**
 *
 * @author
 */
public class Controladores implements ActionListener{
        Datospersonales Datos;
        JFileChooser J;
        Modelos models;

    public Controladores(Datospersonales Datos) {
        super();
        this.Datos = Datos;
        J = new JFileChooser();
        models = new Modelos();
    }

    public Modelos getModels() {
        return models;
    }

    public void setModels(Modelos models) {
        this.models = models;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Guardar":
                 J.showSaveDialog(Datos);
                 models = Datos.obtenerModelos();
                 writeperson(J.getSelectedFile());
                 break;
                 
            case "Seleccionar":
                J.showOpenDialog(Datos);
                models = readperson(J.getSelectedFile());
                Datos.enviarModelos(models);
                break;
                
            case "Limpiar": 
            Datos.clear();
            break;
        }
    }
          public void writeperson(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(models);
            w.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }
    public Modelos readperson(File file){
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Modelos)ois.readObject();
            
        }
        catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
         catch(IOException|ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
        
    
}
