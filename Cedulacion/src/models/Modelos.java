/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author
 */
public class Modelos implements Serializable  {
    private String name;
    private String lastname1;
    private String name2;
    private String lastname2;
    private String cedula;
    private String nacimiento;
    private String edad;

    public Modelos(String name, String lastname1, String name2, String lastname2, String cedula, String nacimiento, String edad) {
        this.name = name;
        this.lastname1 = lastname1;
        this.name2 = name2;
        this.lastname2 = lastname2;
        this.cedula = cedula;
        this.nacimiento = nacimiento;
        this.edad = edad;
    }
    
    public Modelos(){
        this.name = "";
        this.lastname1 = "";
        this.name2 = "";
        this.lastname2 = "";
        this.nacimiento = "";
        this.cedula = "";
        this.edad = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname1() {
        return lastname1;
    }

    public void setLastname1(String lastname1) {
        this.lastname1 = lastname1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getLastname2() {
        return lastname2;
    }

    public void setLastname2(String lastname2) {
        this.lastname2 = lastname2;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }
    
}
