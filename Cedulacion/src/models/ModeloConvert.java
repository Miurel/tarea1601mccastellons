/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author
 */
public class ModeloConvert implements Serializable {
    private String moneda1;
    private String moneda2;
    private String monto1;
    private String monto2;

    public ModeloConvert(String moneda1, String moneda2, String monto1, String monto2) {
        this.moneda1 = moneda1;
        this.moneda2 = moneda2;
        this.monto1 = monto1;
        this.monto2 = monto2;
    }
    
    public ModeloConvert(){
        this.moneda1 = "";
        this.moneda2 = "";
        this.monto1 = "";
        this.monto2 = "";
    }

    public String getMoneda1() {
        return moneda1;
    }

    public void setMoneda1(String moneda1) {
        this.moneda1 = moneda1;
    }

    public String getMoneda2() {
        return moneda2;
    }

    public void setMoneda2(String moneda2) {
        this.moneda2 = moneda2;
    }

    public String getMonto1() {
        return monto1;
    }

    public void setMonto1(String monto1) {
        this.monto1 = monto1;
    }

    public String getMonto2() {
        return monto2;
    }

    public void setMonto2(String monto2) {
        this.monto2 = monto2;
    }
    
}
